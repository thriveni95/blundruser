
import java.util.ArrayList;

public class Tester {
	public static void main(String args[]) {

		ArrayList<BlundrUser> matchedUser;
		ArrayList<BlundrUser> matched1User;
		ArrayList<BlundrUser> matched2User;
		ArrayList<BlundrUser> matched3User;
		ArrayList<BlundrUser> matched4User;
		ArrayList<BlundrUser> matched5User;
		ArrayList<BlundrUser> matched6User;
		ArrayList<BlundrUser> matched7User;
		ArrayList<BlundrUser> matched8User;
		ArrayList<BlundrUser> matched9User;
		ArrayList<BlundrUser> matched10User;
		ArrayList<BlundrUser> matched11User;
		ArrayList<BlundrUser> matched12User;
		

		/*
		 * To Create Qualification class object to call the Qualification Constructor
		 * having parameters
		 * 
		 * @param degree
		 * 
		 * @param college
		 * 
		 * @param passed out year
		 */
		Qualification degree = new Qualification("MASTER OF COMPUTER APPLICATIONS", "ANDHRA UNIVERSITY", "2022");
		Qualification degree2 = new Qualification("MASTER OF Business APPLICATIONS",
				"Mittapalli College Of Engineering", "2017");
		/*
		 * To Create BlundrUserCollection object to call the methods in the
		 * BlundrUserCollecton having method createNewUser method having parameters
		 * 
		 * @param fname
		 * 
		 * @param lname
		 * 
		 * @param dob
		 * 
		 * @param profession
		 * 
		 * @param gender
		 * 
		 * @param marital status
		 * 
		 * @param phone number
		 * 
		 * @param height
		 * 
		 * @param weight
		 * 
		 */
		BlundrUserCollection user = new BlundrUserCollection();
		System.out.println("THE LIST CONTAINS ALL THE INFORMATION ABOUT THE PERSON");

		user.createNewBlundrUser("Thriveni", "Chavala", "1999-05-01", 22, "Software Engineer", BlundrUser.gender.FEMALE,
				BlundrUser.mstatus.SINGLE, "9999999999", 154, 48, "Hyderabad", 25000, BlundrUser.zodiac.CAPRICORN,
				new String[] { "dancing", "swimming", "Cricket" }, new String[] { "Chicken", "Sweets", "Biriyani" },
				new String[] { "Delhi", "hyderabad", "Kerala" }, degree);
		// Qualification degree2=new Qualification("MASTER OF Business
		// APPLICATIONS","Mittapalli College Of Engineering","2017");
		user.createNewBlundrUser("Veera Nageswarao", "Doddaka", "1992-10-24", 27, "Management Trainee",
				BlundrUser.gender.MALE, BlundrUser.mstatus.SINGLE, "1111111111", 168, 70, "Hyderabad", 45000,
				BlundrUser.zodiac.LIBRA, new String[] { "cooking", "planting", "Travelling", "playing cricket" },
				new String[] { "Chicken", "Sweets", "Biriyani", "pickles", "fish fry" },
				new String[] { "Delhi", "Bangloore", "Kakinada" }, degree2);
		/*
		 * To print the user information by using findMatchByUser method which accepts
		 * the username as a parameter
		 */
		matchedUser = user.findMatchByName("");
		for (BlundrUser name : matchedUser)
			System.out.println(name);// To print the matched user information
		/*
		 * To print the user information by using findMatchByHobbie method which accepts
		 * the username as a parameter
		 */
		matchedUser = user.findMatchByHobbie("cooking");
		for (BlundrUser Hobbie : matchedUser)
			System.out.println(Hobbie);// To print the matched user information
		/*
		 * To print the user information by using findMatchByHobbies method which
		 * accepts the String of Hobbies as a parameter
		 */
		System.out.println("print the list of users having the hobbies");
		System.out.println("...................................................");
		matched1User = user.findMatchByHobbiesOr(new String[] { "cooking", "Cricket", "Travelling" });
		for (BlundrUser Hobbies : matched1User)
			System.out.println(Hobbies);
		/*
		 * To print the user information by using findMatchByFood method which accepts
		 * the item as a parameter
		 */
		matched6User = user.findMatchByFood("");
		for (BlundrUser Food : matched6User)
			System.out.println(Food);// To print the matched user information
		/*
		 * To print the user information by using findMatchByFoodOr method which accepts
		 * the String of items as a parameter
		 */
		System.out.println("print the list of users having the food items");
		System.out.println("...................................................");
		matched7User = user.findMatchByFoodOr(new String[] {});
		for (BlundrUser Foods : matched7User)
			System.out.println(Foods);
		/*
		 * To print the user information by using findMatchByLocation method which
		 * accepts the location as a parameter
		 */
		System.out.println("print the list of users having the search location");
		System.out.println(".......................................................");
		matched2User = user.findMatchByLocation("");
		for (BlundrUser Location : matched2User)
			System.out.println(Location);
		/*
		 * To print the user information by using findMatchByHeightMin method which
		 * accepts the height as a parameter
		 */
		System.out.println("print the list of users having the minimum height");
		System.out.println(".......................................................");
		matched3User = user.findMatchByHeightMin(170);
		for (BlundrUser height : matched3User)
			System.out.println(height);
		/*
		 * To print the user information by using findMatchByHeightMax method which
		 * accepts the height as a parameter
		 */
		System.out.println("print the list of users having the maximum height");
		System.out.println(".......................................................");
		matched4User = user.findMatchByHeightMax(155);
		for (BlundrUser height : matched4User)
			System.out.println(height);
		/*
		 * To print the user information by using findMatchByHeightBetween method which
		 * accepts the height as a parameter
		 */
		System.out.println("print the list of users having the  height Between min and Max");
		System.out.println(".......................................................");
		matched5User = user.findMatchByHeightBetween(155, 170);
		for (BlundrUser height : matched5User)
			System.out.println(height);
		/*
		 * To print the user information by using findMatchByplace method which accepts
		 * the place as a parameter
		 */
		matched8User = user.findMatchByPlace("");
		for (BlundrUser food : matched8User)
			System.out.println(food);// To print the matched user information
		/*
		 * To print the user information by using findMatchByPlaces method which accepts
		 * the String of favourite places as a parameter
		 */
		System.out.println("print the list of users having the favourite places");
		System.out.println("...................................................");
		matched9User = user.findMatchByPlacesOr(new String[] {""});
		for (BlundrUser places : matched9User)
			System.out.println(places);
		/*
		 * To print the user information by using findMatchByDegreeString method which
		 * accepts the String of degree as a parameter
		 */
		System.out.println("print the list of users having the degree");
		System.out.println("...................................................");
		matched10User = user.findMatchByDegreeString("MASTER OF COMPUTER APPLICATIONS");
		for (BlundrUser Degree : matched10User)
			System.out.println(Degree);
		/*
		 * To print the user information by using findMatchByPlacesAnd method which
		 * accepts the String of places as a parameter
		 */
		System.out.println("print the list of users having the favourite places");
		System.out.println("...................................................");
		matched11User = user.findMatchByPlacesAnd(new String[] {"kerala","Bangloore"});
		for (BlundrUser places : matched11User)
			System.out.println(places);
		/*
		 * To print the user information by using findMatchByDegreeOr method which
		 * accepts the String of degree,college,passedout year as a parameter
		 */
		System.out.println("print the list of users having the same degree,college and passed out year");
		System.out.println("...................................................");
		matched12User = user.findMatchByDegreeOr(new String[] {"Mittapalli College Of Engineering"});
		for (BlundrUser Degre : matched12User)
			System.out.println(Degre);
	}
}
