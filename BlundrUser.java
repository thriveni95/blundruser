
import java.util.ArrayList;

public class BlundrUser 
{
	/*
	 * To create BlundrUser class to display all the users having 
	 * first name,last name,date of birth,age,profession,gender, marital status,phone number,height,weight,location,salary,zodiac sign
	 * qualification,hobbies,favourite food,favourite places
	 * @author Thriveni
	 */
    public String fname;
    public String lname;
    public String dob;
    public int age;
    public String profession;
    public gender aGender;
    public mstatus aMstatus;
    public String ph_number;
    public int height;
    public int weight;
    public String location;
    public int salary;
    public Qualification quali;
    public zodiac aZodiac;
    public String[] hobbies=new String[10];
    public String[] food=new String[10];
    public String[] places=new String[10];
    //ArrayList<Qualification>degree=new ArrayList<Qualification>();
    /*
     * To create enum methods for
     * gender
     * mstatus
     * zodiac
     */
    public enum gender{
    	MALE,
    	FEMALE;
    }
    public enum mstatus{
    	SINGLE,
    	MARRIED,
    	WIDOWED,
    	DIVORCED,
    	SEPERATED;
    }
    public enum zodiac{
    	ARIES,TAURUS,GEMINI,CANCER,LEO,VIRGO,LIBRA,SCORPIO,SAGITTARIUS,CAPRICORN,AQUARIUS,PISCES;
    }
    /**
	 * To create a BlundrUser constructor with arguments 
	 * @param fname
	 * @param age
	 * @param profession
	 * @param hobbies
	 * @param food
	 * @param places
	 */
    public BlundrUser(String fname,String lname,String dob,int age,String profession,gender aGender,mstatus aMstatus,
    		          String ph_number, int height,int weight,String location,int salary,zodiac aZodiac,String[] hobbies,
    		          String[] food,String[] places,Qualification quali)
    {
    	this.fname=fname;            //using this statement
    	this.lname=lname;            //using this statement
    	this.dob  =dob;              //using this statement
    	this.age  =age;              //using this statement
    	this.profession=profession;  //using this statement
    	this.aGender=aGender;          //using this statement
    	this.aMstatus=aMstatus;        //using this statement
    	this.ph_number=ph_number;    //using this statement
    	this.height=height;          //using this statement
    	this.weight=weight;           //using this statement
    	this.location=location;       //using this statement
    	this.salary=salary;          //using this statement
    	this.aZodiac=aZodiac;          //using this statement
    	this.hobbies=hobbies;        //using this statement
    	this.food=food;              //using this statement
    	this.places=places;          //using this statement
    	this.quali=quali;
    	System.out.println(this);
    }
    /**
	 * override the default toString method of Message so that message can be
	 * printed in human readable format
	 */
    @Override
	public String toString() {
		String StringToReturn = "";
		StringToReturn +="Name                           :" + this.fname +this.lname+          "\n";
		StringToReturn +="Date Of Birth                  :" + this.dob+                          "\n";
		StringToReturn +="Age                            :" + this.age +                        "\n";
		StringToReturn +="Profession                     :" + this.profession +                 "\n";
		StringToReturn +="Gender                         :" + this.aGender +                     "\n";
		StringToReturn +="Marital Status                 :" + this.aMstatus +                 "\n";
		StringToReturn +="Phone-Number                   :" + this.ph_number +    "\n";
		StringToReturn +="Height                         :" + this.height +    "\n";
		StringToReturn +="Weight                         :" + this.weight +    "\n";
		StringToReturn +="Location                       :" + this.location +    "\n";
		StringToReturn +="Salary                         :" + this.salary +    "\n";
		StringToReturn +="Zodiac Sign                    :" + this.aZodiac +    "\n";
		String hobbyString = "";
		for (int i = 0; i < this.hobbies.length; i++) {
			hobbyString += "\"" + this.hobbies[i] + "\" ,";
		}
		hobbyString += "";

		StringToReturn +="hobbies                        :"+ hobbyString + "\n";
		String foodString = "";
		for (int i = 0; i < this.food.length; i++) {
			foodString += "\"" + this.food[i] + "\", ";
		}
		foodString += "";

		StringToReturn +="Favourite Food Items           :"+ foodString + "\n";
		String placeString = "";
		for (int i = 0; i < this.places.length; i++) {
			placeString += "\"" + this.places[i] + "\", ";
		}
		placeString += "";

		StringToReturn +="Favourite Places               :" + placeString + "\n";
		StringToReturn +="Qualification                  :" + this.quali +    "\n";
		StringToReturn +="........................................................";
		
		return StringToReturn;
	}
	}
    
    

