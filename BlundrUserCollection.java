
import java.sql.Array;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

public class BlundrUserCollection {
	/**
	 * To create a BlundrUserCollection class to implement the UserCollection by
	 * using ArrayList
	 * 
	 * @author Thriveni
	 */
	ArrayList<BlundrUser> userList = new ArrayList<BlundrUser>();

	/**
	 * To create createNewBlundrUser method using parameters by using ArrayList and
	 * addUser
	 * 
	 * @param name
	 * @param age
	 * @param profession
	 * @param hobbie
	 * @param hobbies
	 */
	public boolean createNewBlundrUser(String fname, String lname, String dob, int age, String profession,
			BlundrUser.gender aGender, BlundrUser.mstatus aMstatus, String ph_number, int height, int weight,
			String location, int salary, BlundrUser.zodiac aZodiac, String[] hobbies, String[] food, String[] places,
			Qualification quali) {
		BlundrUser adduser = new BlundrUser(fname, lname, dob, age, profession, aGender, aMstatus, ph_number, height,
				weight, location, salary, aZodiac, hobbies, food, places, quali);
		this.userList.add(adduser);
		return true;
	}

	/*
	 * To create a method named as ** ...findMatchByUser...* which accepts the
	 * 
	 * @param username used to search the user by name
	 */
	public ArrayList<BlundrUser> findMatchByName(String username) {
		ArrayList<BlundrUser> searchList = new ArrayList<BlundrUser>();
		BlundrUser foundUser = null;
		for (BlundrUser currentUser : userList) {
			if (currentUser.fname == username) {
				foundUser = currentUser;
				searchList.add(foundUser);
			}
		}
		return searchList;

	}

	/*
	 * To create a method named as ** ...findMatchByHobbie...* which accepts the
	 * 
	 * @param String hobbie used to search the user by Hobbies
	 */
	public ArrayList<BlundrUser> findMatchByHobbie(String hobbie) {
		ArrayList<BlundrUser> searchList = new ArrayList<BlundrUser>();

		for (int a = 0; a < this.userList.size(); a++) {
			BlundrUser currentUser = this.userList.get(a);
			for (int b = 0; b < currentUser.hobbies.length; b++) {
				if (currentUser.hobbies[b] == (hobbie)) {
					searchList.add(currentUser);

				}
			}
		}
		return searchList;
	}

	/*
	 * To create a method named as ** ...findMatchByHobbies...* which accepts the
	 * 
	 * @param String[] hobbies used to search the user by Hobbies
	 */
	public ArrayList<BlundrUser> findMatchByHobbiesOr(String[] hobbies1) {
		HashSet<BlundrUser> searchList = new HashSet<BlundrUser>();
		// ArrayList<BlundrUser>searchList1=new ArrayList<BlundrUser>(searchList);
		for (int a = 0; a < this.userList.size(); a++) {
			BlundrUser currentUser = this.userList.get(a);
			for (int b = 0; b < currentUser.hobbies.length; b++) {
				for (int i = 0; i < hobbies1.length; i++) {
					if (currentUser.hobbies[b] == (hobbies1[i])) {
						searchList.add(currentUser);

					}
				}
			}
		}

		return new ArrayList<BlundrUser>(searchList);
	}

	/*
	 * To create a method named as ** ...findMatchByFood...* which accepts the
	 * 
	 * @param String item used to search the user by item
	 */
	public ArrayList<BlundrUser> findMatchByFood(String item) {
		ArrayList<BlundrUser> searchList = new ArrayList<BlundrUser>();

		for (int a = 0; a < this.userList.size(); a++) {
			BlundrUser currentUser = this.userList.get(a);
			for (int b = 0; b < currentUser.food.length; b++) {
				if (currentUser.food[b] == (item)) {
					searchList.add(currentUser);

				}
			}
		}
		return searchList;
	}

	/*
	 * To create a method named as ** ...findMatchByFoodOr...* which accepts the
	 * 
	 * @param String[] items used to search the user by items
	 */
	public ArrayList<BlundrUser> findMatchByFoodOr(String[] items) {
		HashSet<BlundrUser> searchList = new HashSet<BlundrUser>();

		for (int a = 0; a < this.userList.size(); a++) {
			BlundrUser currentUser = this.userList.get(a);
			for (int b = 0; b < currentUser.food.length; b++) {
				for (int i = 0; i < items.length; i++) {
					if (currentUser.hobbies[b] == (items[i])) {
						searchList.add(currentUser);

					}
				}
			}
		}

		return new ArrayList<BlundrUser>(searchList);
	}

	/*
	 * To create a method named as ** ...findMatchByPlace...* which accepts the
	 * 
	 * @param String place used to search the user by place
	 */
	public ArrayList<BlundrUser> findMatchByPlace(String place) {
		ArrayList<BlundrUser> searchList = new ArrayList<BlundrUser>();

		for (int a = 0; a < this.userList.size(); a++) {
			BlundrUser currentUser = this.userList.get(a);
			for (int b = 0; b < currentUser.places.length; b++) {
				if (currentUser.places[b] == place) {
					searchList.add(currentUser);

				}
			}
		}
		return searchList;
	}

	/*
	 * To create a method named as ** ...findMatchByPlacesOrOr...* which accepts the
	 * 
	 * @param String[] places used to search the user by places
	 */
	public ArrayList<BlundrUser> findMatchByPlacesOr(String[] places) {
		HashSet<BlundrUser> searchList = new HashSet<BlundrUser>();

		for (int a = 0; a < this.userList.size(); a++) {
			BlundrUser currentUser = this.userList.get(a);
			for (int b = 0; b < currentUser.places.length; b++) {
				for (int i = 0; i < places.length; i++) {
					if (currentUser.hobbies[b] == (places[i])) {
						searchList.add(currentUser);

					}
				}
			}
		}

		return new ArrayList<BlundrUser>(searchList);
	}

	/*
	 * To create a method named as ** ...findMatchByPlacesOrOr...* which accepts the
	 * 
	 * @param String[] places used to search the user by places
	 */
	public ArrayList<BlundrUser> findMatchByPlacesAnd(String[] place) {
		HashSet<BlundrUser> searchList = new HashSet<BlundrUser>();

		for (int a = 0; a < this.userList.size(); a++) {
			BlundrUser currentUser = this.userList.get(a);
			List<String> foundUser = Arrays.asList(currentUser.places);
			@SuppressWarnings("unused")
			boolean areAllFound = true;
			for (int i = 0; i < place.length; i++) {
				for (int j = 0; j < foundUser.size(); j++) {
					if (!foundUser.contains(place[i])) {
						areAllFound = false;
					}
					if (areAllFound = true) {
						searchList.add(currentUser);
					}
				}
			}
		}

		return new ArrayList<BlundrUser>(searchList);
	}

	/*
	 * To create a method named as ** ...findMatchByLocation...* which accepts the
	 * 
	 * @param String[] hobbies used to search the user by Hobbies
	 */
	public ArrayList<BlundrUser> findMatchByLocation(String location) {
		ArrayList<BlundrUser> searchList = new ArrayList<BlundrUser>();
		BlundrUser foundUser = null;
		for (BlundrUser currentUser : userList) {
			if (currentUser.location == location) {
				foundUser = currentUser;
				searchList.add(foundUser);
			}
		}
		return searchList;
	}

	/*
	 * To create a method named as ** ...findMatchByHeightMin...* which accepts the
	 * 
	 * @param String[] hobbies used to search the user by Hobbies
	 */
	public ArrayList<BlundrUser> findMatchByHeightMin(int height) {
		ArrayList<BlundrUser> searchList = new ArrayList<BlundrUser>();
		BlundrUser foundUser = null;
		for (BlundrUser currentUser : userList) {
			if (currentUser.height < height) {
				foundUser = currentUser;
				searchList.add(foundUser);
			}
		}
		return searchList;
	}

	/*
	 * To create a method named as ** ...findMatchByHeightMax...* which accepts the
	 * 
	 * @param String[] hobbies used to search the user by Hobbies
	 */
	public ArrayList<BlundrUser> findMatchByHeightMax(int height) {
		ArrayList<BlundrUser> searchList = new ArrayList<BlundrUser>();
		BlundrUser foundUser = null;
		for (BlundrUser currentUser : userList) {
			if (currentUser.height > height) {
				foundUser = currentUser;
				searchList.add(foundUser);
			}
		}
		return searchList;
	}

	/*
	 * To create a method named as ** ...findMatchByHeightBetween...* which accepts
	 * the
	 * 
	 * @param String[] hobbies used to search the user by Hobbies
	 */
	public ArrayList<BlundrUser> findMatchByHeightBetween(int minheight, int maxheight) {
		ArrayList<BlundrUser> searchList = new ArrayList<BlundrUser>();
		BlundrUser foundUser = null;
		for (BlundrUser currentUser : userList) {
			if (currentUser.height >= minheight && currentUser.height <= maxheight) {
				foundUser = currentUser;
				searchList.add(foundUser);
			}
		}
		return searchList;
	}

	/*
	 * To create a method named as ** ...findMatchByweightMin...* which accepts the
	 * 
	 * @param String[] hobbies used to search the user by Hobbies
	 */
	public ArrayList<BlundrUser> findMatchByWeightMin(int weight) {
		ArrayList<BlundrUser> searchList = new ArrayList<BlundrUser>();
		BlundrUser foundUser = null;
		for (BlundrUser currentUser : userList) {
			if (currentUser.weight < weight) {
				foundUser = currentUser;
				searchList.add(foundUser);
			}
		}
		return searchList;
	}

	/*
	 * To create a method named as ** ...findMatchByweightMax...* which accepts the
	 * 
	 * @param String[] hobbies used to search the user by Hobbies
	 */
	public ArrayList<BlundrUser> findMatchByweightMax(int weight) {
		ArrayList<BlundrUser> searchList = new ArrayList<BlundrUser>();
		BlundrUser foundUser = null;
		for (BlundrUser currentUser : userList) {
			if (currentUser.weight > weight) {
				foundUser = currentUser;
				searchList.add(foundUser);
			}
		}
		return searchList;
	}

	/*
	 * To create a method named as ** ...findMatchByweightBetween...* which accepts
	 * the
	 * 
	 * @param String[] hobbies used to search the user by Hobbies
	 */
	public ArrayList<BlundrUser> findMatchByweightBetween(int minweight, int maxweight) {
		ArrayList<BlundrUser> searchList = new ArrayList<BlundrUser>();
		BlundrUser foundUser = null;
		for (BlundrUser currentUser : userList) {
			if (currentUser.weight >= minweight && currentUser.weight <= maxweight) {
				foundUser = currentUser;
				searchList.add(foundUser);
			}
		}
		return searchList;
	}

	/*
	 * To create a method named as ** ...findMatchByAgeMin...* which accepts the
	 * 
	 * @param String[] hobbies used to search the user by Hobbies
	 */
	public ArrayList<BlundrUser> findMatchByAgetMin(int age) {
		ArrayList<BlundrUser> searchList = new ArrayList<BlundrUser>();
		BlundrUser foundUser = null;
		for (BlundrUser currentUser : userList) {
			if (currentUser.age < age) {
				foundUser = currentUser;
				searchList.add(foundUser);
			}
		}
		return searchList;
	}

	/*
	 * To create a method named as ** ...findMatchByAgeMax...* which accepts the
	 * 
	 * @param String[] hobbies used to search the user by Hobbies
	 */
	public ArrayList<BlundrUser> findMatchByAgeMax(int age) {
		ArrayList<BlundrUser> searchList = new ArrayList<BlundrUser>();
		BlundrUser foundUser = null;
		for (BlundrUser currentUser : userList) {
			if (currentUser.age > age) {
				foundUser = currentUser;
				searchList.add(foundUser);
			}
		}
		return searchList;
	}

	/*
	 * To create a method named as ** ...findMatchByAgeBetween...* which accepts the
	 * 
	 * @param String[] hobbies used to search the user by Hobbies
	 */
	public ArrayList<BlundrUser> findMatchByAgeBetween(int minage, int maxage) {
		ArrayList<BlundrUser> searchList = new ArrayList<BlundrUser>();
		BlundrUser foundUser = null;
		for (BlundrUser currentUser : userList) {
			if (currentUser.age >= minage && currentUser.age <= maxage) {
				foundUser = currentUser;
				searchList.add(foundUser);
			}
		}
		return searchList;
	}

	/*
	 * To create a method named as ** ...findMatchBySalaryMin...* which accepts the
	 * 
	 * @param String[] hobbies used to search the user by Hobbies
	 */
	public ArrayList<BlundrUser> findMatchBySalaryMin(int salary) {
		ArrayList<BlundrUser> searchList = new ArrayList<BlundrUser>();
		BlundrUser foundUser = null;
		for (BlundrUser currentUser : userList) {
			if (currentUser.salary < salary) {
				foundUser = currentUser;
				searchList.add(foundUser);
			}
		}
		return searchList;
	}

	/*
	 * To create a method named as ** ...findMatchBySalaryMax...* which accepts the
	 * 
	 * @param String[] hobbies used to search the user by Hobbies
	 */
	public ArrayList<BlundrUser> findMatchBySalaryMax(int salary) {
		ArrayList<BlundrUser> searchList = new ArrayList<BlundrUser>();
		BlundrUser foundUser = null;
		for (BlundrUser currentUser : userList) {
			if (currentUser.salary > salary) {
				foundUser = currentUser;
				searchList.add(foundUser);
			}
		}
		return searchList;
	}

	/*
	 * To create a method named as ** ...findMatchBySalaryBetween...* which accepts
	 * the
	 * 
	 * @param String[] hobbies used to search the user by Hobbies
	 */
	public ArrayList<BlundrUser> findMatchBySalaryBetween(int minsalary, int maxsalary) {
		ArrayList<BlundrUser> searchList = new ArrayList<BlundrUser>();
		BlundrUser foundUser = null;
		for (BlundrUser currentUser : userList) {
			if (currentUser.salary >= minsalary && currentUser.salary <= maxsalary) {
				foundUser = currentUser;
				searchList.add(foundUser);
			}
		}
		return searchList;
	}

	/*
	 * To create a method named as ** ...findMatchByDegree...* which accepts the
	 * 
	 * @param object degree used to search the user by degree
	 */
	public ArrayList<BlundrUser> findMatchByDegreeString(String degree) {
		ArrayList<BlundrUser> searchList = new ArrayList<BlundrUser>();

		for (int a = 0; a < this.userList.size(); a++) {
			BlundrUser currentUser = this.userList.get(a);
			if (currentUser.quali.degree == degree) {
				searchList.add(currentUser);

			}
		}
		return searchList;
	}

	/*
	 * To create a method named as ** ...findMatchByGender...* which accepts the
	 * 
	 * @param String gender used to search the user by gender
	 */
	/*public ArrayList<BlundrUser> findMatchByGender(String gender) {
		ArrayList<BlundrUser> searchList = new ArrayList<BlundrUser>();
		BlundrUser foundUser = null;
		for (BlundrUser currentUser : userList) {
			//if (currentUser.aGender == gender) {
				foundUser = currentUser;
				searchList.add(foundUser);
			}
		}

		return userList;

	/*
	 * To create a method named as ** ...findMatchByDegreeOr...* which accepts the
	 * 
	 * @param String[] degree1 used to search the user by degree
	 */
	public ArrayList<BlundrUser> findMatchByDegreeOr(String[] degree1) {
		HashSet<BlundrUser> searchList = new HashSet<BlundrUser>();
		for (int a = 0; a < this.userList.size(); a++) {
			BlundrUser currentUser = this.userList.get(a);
			for (int i = 0; i < degree1.length; i++) {
				if (currentUser.quali.degree == degree1[i] || currentUser.quali.clg == degree1[i]
						|| currentUser.quali.year == degree1[i]) {
					searchList.add(currentUser);
				}
			}
		}
		return new ArrayList<>(searchList);
	}
}
