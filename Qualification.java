
public class Qualification {
public String degree;
public String clg;
public String year;
public Qualification(String degree,String clg,String year)
{
	this.degree=degree;
	this.clg=clg;
	this.year=year;

}
/**
 * override the default toString method of Message so that message can be
 * printed in human readable format
 */
@Override
public String toString() {
	//return "Qualification [degree=" + degree + ", clg=" + clg + ", year=" + year + "]";
	String StringToReturn = "";
	StringToReturn +="Highest Qualification    :      " + this.degree+           "\n";
	StringToReturn +="                                "+"College                  :      " +this.clg+               "\n";
	StringToReturn +="                                "+"Passed out year          :      "+this.year+               "\n" ; 
	return StringToReturn;
}


}